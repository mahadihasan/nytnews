package com.hasan.app.nytnews;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;
import android.databinding.DataBindingUtil;


import com.hasan.app.nytnews.adapters.NewsItemAdapter;
import com.hasan.app.nytnews.databinding.ActivityMainBinding;
import com.hasan.app.nytnews.interfaces.MainView;
import com.hasan.app.nytnews.interfaces.RecyclerViewItemClickListener;
import com.hasan.app.nytnews.models.Article;
import com.hasan.app.nytnews.view_models.MainViewModel;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, MainView {

    private MainViewModel viewModel;
    private RecyclerView mRecyclerView;
    private NewsItemAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private SwipeRefreshLayout srLayout;
    private boolean mLoading = false;
    private int pageNumber = 0;
    private final String TAG = this.getClass().getSimpleName();

    private ArrayList<Article> mArticles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        srLayout = binding.srLayout;
        srLayout.setOnRefreshListener(this);

        mArticles = new ArrayList<>();

        mRecyclerView = binding.rvNews;
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        mAdapter = new NewsItemAdapter(MainActivity.this, mArticles);
        mAdapter.setItemClickListener(new ItemClickListener());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new ScrollListener());

        viewModel = new MainViewModel(this);

        srLayout.setRefreshing(true);
        viewModel.searchArticles(pageNumber);
    }

    @Override
    public void onRefresh() {
        pageNumber = 0;
        srLayout.setRefreshing(true);
        viewModel.searchArticles(pageNumber);
    }

    @Override
    public void showError() {
        Toast.makeText(getApplicationContext(), getString(R.string.error_generic), Toast.LENGTH_SHORT);
    }


    /**
     * Updates the list view in the UI with new article list
     *
     * @param articles New article list got from server
     */
    @Override
    public void updateListView(ArrayList<Article> articles) {
        if (articles != null) {
            if (pageNumber == 0) {
                mArticles.clear();
            }

            mArticles.addAll(articles);
            mAdapter.setItems(mArticles);
            mAdapter.notifyDataSetChanged();

            srLayout.setRefreshing(false);
        }

    }

    private class ScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int totalItem = mLayoutManager.getItemCount();
            int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

            if (!mLoading && lastVisibleItem == totalItem - 1) {
                mLoading = true;

                pageNumber += 1;
                viewModel.searchArticles(pageNumber);

                mLoading = false;
            }
        }
    }

    private class ItemClickListener implements RecyclerViewItemClickListener {
        @Override
        public void onItemClick(Article article) {

            Log.d(TAG, article.toString());

            Intent intent = new Intent(getBaseContext(), WebViewActivity.class);
            intent.putExtra("URL", article.getWebUrl());
            intent.putExtra("TITLE", article.getSubsectionName() != null ? article.getSubsectionName() : article.getSectionName());
            startActivity(intent);

        }
    }
}
