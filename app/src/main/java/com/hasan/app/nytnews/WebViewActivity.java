package com.hasan.app.nytnews;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.databinding.DataBindingUtil;
import android.widget.ProgressBar;

import com.hasan.app.nytnews.databinding.ActivityWebViewBinding;

public class WebViewActivity extends AppCompatActivity {

    //Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityWebViewBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);

        //toolbar = binding.toolbar;
        binding.toolbarTitle.setText("");


        String url = getIntent().getStringExtra("URL");
        String title = getIntent().getStringExtra("TITLE");

        if(title!=null)
        {
            binding.toolbarTitle.setText(title);
        }

        if(url!=null)
        {
            binding.webView.setWebViewClient(new CustomWebViewClient(binding.progressBar));
            binding.webView.loadUrl(url);
        }

        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    private class CustomWebViewClient extends WebViewClient
    {
        ProgressBar mProgressBar;
        public CustomWebViewClient(ProgressBar progressBar) {
            mProgressBar = progressBar;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);

            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            mProgressBar.setVisibility(View.GONE);
        }
    }

}
