package com.hasan.app.nytnews.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hasan.app.nytnews.R;
import com.hasan.app.nytnews.interfaces.ItemTouchListener;
import com.hasan.app.nytnews.interfaces.RecyclerViewItemClickListener;
import com.hasan.app.nytnews.models.Article;
import com.hasan.app.nytnews.models.Multimedia;
import com.hasan.app.nytnews.utility.Constants;
import com.hasan.app.nytnews.utility.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by mahadi on 17.06.17.
 */

public class NewsItemAdapter extends RecyclerView.Adapter<NewsItemAdapter.ItemViewHolder> {

    Context mContext;
    ArrayList<Article> mArticles;
    private String TAG = this.getClass().getSimpleName();


    private RecyclerViewItemClickListener itemClickListener;

    public NewsItemAdapter(Context context, ArrayList<Article> articles) {
        mContext = context;
        mArticles = articles;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_news, parent, false);
        ItemViewHolder viewHolder = new ItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

        final Article article = mArticles.get(position);

        if (article != null) {
            try {
                holder.tvHeadline.setText(article.getHeadline().getMain());
                holder.tvSnippet.setText(article.getSnippet());
                holder.tvTime.setText(Utils.getFormattedTimeDifference(article.getPubDate(), mContext.getString(R.string.ago)));
                holder.ivArticle.setVisibility(View.GONE);

                String url = getUrl(article);
                if (url != null) {
                    holder.ivArticle.setVisibility(View.VISIBLE);
                    Picasso.with(mContext).load(url).into(holder.ivArticle);
                }

                holder.cardView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        itemClickListener.onItemClick(article);
                    }
                });


            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }

        }


    }

    @Override
    public int getItemCount() {
        return mArticles.size();
    }

    public void setItems(ArrayList<Article> articles) {
        this.mArticles = articles;
    }

    public void setItemClickListener(RecyclerViewItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    private String getUrl(Article article) {
        String url = null;

        if (article.getMultimedia() != null) {
            for (Multimedia multimedia : article.getMultimedia()) {
                if (multimedia.getSubtype().equalsIgnoreCase("wide")) {

                    if (multimedia.getUrl() != null) {
                        url = Constants.SITE_URL + multimedia.getUrl();
                    }
                    break;
                }

            }

        }

        return url;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder implements ItemTouchListener {


        protected TextView tvHeadline;
        protected TextView tvSnippet;
        protected TextView tvTime;
        protected ImageView ivArticle;
        protected CardView cardView;

        public ItemViewHolder(View view) {
            super(view);

            this.tvHeadline = (TextView) view.findViewById(R.id.tv_headline);
            this.tvSnippet = (TextView) view.findViewById(R.id.tv_snippet);
            this.tvTime = (TextView) view.findViewById(R.id.tv_time);
            this.ivArticle = (ImageView) view.findViewById(R.id.iv_article);
            this.cardView = (CardView) view.findViewById(R.id.card_view);
        }

        @Override
        public void onItemSelected() {

        }

        @Override
        public void onItemClear() {

        }
    }
}
