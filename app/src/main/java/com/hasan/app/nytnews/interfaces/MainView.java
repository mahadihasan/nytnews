package com.hasan.app.nytnews.interfaces;

import com.hasan.app.nytnews.models.Article;
import com.hasan.app.nytnews.models.ArticleSearch;

import java.util.ArrayList;

/**
 * Created by mahadi on 18.06.17.
 */

public interface MainView {
    void showError();
    void updateListView(ArrayList<Article> articles);
}
