package com.hasan.app.nytnews.interfaces;

import com.hasan.app.nytnews.models.Article;

/**
 * Created by mahadi on 18.06.17.
 */

public interface RecyclerViewItemClickListener {
    void onItemClick(Article article);
}
