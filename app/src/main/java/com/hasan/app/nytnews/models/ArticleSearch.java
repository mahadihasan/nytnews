package com.hasan.app.nytnews.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mahadi on 17.06.17.
 */

public class ArticleSearch {

    @SerializedName("response")
    @Expose
    private Response response;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("copyright")
    @Expose
    private String copyright;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    @Override
    public String toString() {
        return "ArticleSearch{" +
                "response=" + response +
                ", status='" + status + '\'' +
                ", copyright='" + copyright + '\'' +
                '}';
    }
}
