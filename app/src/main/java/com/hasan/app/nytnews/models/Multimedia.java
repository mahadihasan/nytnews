package com.hasan.app.nytnews.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mahadi on 17.06.17.
 */

public class Multimedia {

    @SerializedName("width")
    @Expose
    private int width;

    @SerializedName("height")
    @Expose
    private int main;

    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("rank")
    @Expose
    private int rank;

    @SerializedName("subtype")
    @Expose
    private String subtype;

    @SerializedName("type")
    @Expose
    private String type;


    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getMain() {
        return main;
    }

    public void setMain(int main) {
        this.main = main;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Multimedia{" +
                "width=" + width +
                ", main=" + main +
                ", url='" + url + '\'' +
                ", rank=" + rank +
                ", subtype='" + subtype + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
