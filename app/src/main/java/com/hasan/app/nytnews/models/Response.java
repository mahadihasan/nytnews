package com.hasan.app.nytnews.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mahadi on 17.06.17.
 */

public class Response {

    @SerializedName("meta")
    @Expose
    private Meta meta;

    @SerializedName("docs")
    @Expose
    private ArrayList<Article> articles;

    private int currentPage;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<Article> articles) {
        this.articles = articles;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public String toString() {
        return "Response{" +
                "meta=" + meta +
                ", articles=" + articles +
                '}';
    }
}
