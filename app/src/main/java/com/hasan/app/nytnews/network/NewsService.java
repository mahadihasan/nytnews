package com.hasan.app.nytnews.network;

import com.hasan.app.nytnews.models.ArticleSearch;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by mahadi on 17.06.17.
 */

public interface NewsService {

    @GET("articlesearch.json")
    Call<ArticleSearch> getArticles(@Query("api-key") String apiKey, @Query("page") int page);

}
