package com.hasan.app.nytnews.network;

/**
 * Created by mahadi on 17.06.17.
 */

public class RestUtils {
    public static final String API_KEY = "d7d008858ee64d3fa5f8d8b6a839b435";
    public static final String BASE_URL = "https://api.nytimes.com/svc/search/v2/";

    public static NewsService getNewsService() {
        return RestClient.getClient(BASE_URL).create(NewsService.class);
    }
}
