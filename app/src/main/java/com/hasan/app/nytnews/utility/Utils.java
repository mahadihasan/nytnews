package com.hasan.app.nytnews.utility;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by mahadi on 17.06.17.
 */

public class Utils {

    /**
     * Converts a UTC date string to local formatted time and returns the difference in mins, hours or days.
     *
     * @param dateString UTC date string
     * @param postfix String to append at the end of the time value
     * @return      Formatted time difference
     */
    public static String getFormattedTimeDifference(String dateString, String postfix)
    {
        String output = "";

        int minsInHour = 60;
        int minsInDay = 60*24;

        try
        {
            DateTime dateTime = new DateTime(dateString, DateTimeZone.getDefault());
            DateTime currentDateTime = new DateTime();

            long diffMins = (currentDateTime.getMillis()-dateTime.getMillis())/(1000*60);

            if(diffMins<minsInHour)
            {
                //min
                output = diffMins+"m"+postfix;
            }
            else if(diffMins<minsInDay)
            {
                //hour
                output = (int)(diffMins/minsInHour)+"h"+postfix;
            }
            else
            {
                //day
                //output = (int)(diffMins/minsInDay)+"d";
                output = dateTime.toString("MMM dd");
            }
        }
        catch (Exception e)
        {

        }



        return output;
    }

}
