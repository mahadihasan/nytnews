package com.hasan.app.nytnews.view_models;

import android.app.Activity;
import android.databinding.BaseObservable;
import android.util.Log;
import android.widget.Toast;

import com.hasan.app.nytnews.MainActivity;
import com.hasan.app.nytnews.R;
import com.hasan.app.nytnews.interfaces.MainView;
import com.hasan.app.nytnews.models.Article;
import com.hasan.app.nytnews.models.ArticleSearch;
import com.hasan.app.nytnews.network.NewsService;
import com.hasan.app.nytnews.network.RestUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahadi on 18.06.17.
 */

public class MainViewModel extends BaseObservable {

    MainView mMainView;

    public MainViewModel(MainView mainView) {
        mMainView = mainView;
    }

    /**
     * Calls getArticles API and fetch articles from server.
     * Updates the existing articles and notifies main view to update list with new data.
     *
     * @param pageNumber Page number for the news articles
     */
    public void searchArticles(final int pageNumber) {
        NewsService newsService = RestUtils.getNewsService();

        newsService.getArticles(RestUtils.API_KEY, pageNumber).enqueue(new Callback<ArticleSearch>() {
            @Override
            public void onResponse(Call<ArticleSearch> call, Response<ArticleSearch> response) {

                if (response.isSuccessful()) {

                    ArticleSearch articleSearch = response.body();
                    mMainView.updateListView(getArticles(articleSearch));

                } else {
                    //show error
                    mMainView.showError();
                }
            }

            @Override
            public void onFailure(Call<ArticleSearch> call, Throwable t) {
                //show error
                mMainView.showError();
            }
        });
    }

    /**
     * Returns article list from ArticleSearch object
     *
     * @param articleSearch ArticleSearch object which contains the article list
     * @return      New article list from articleSearch object
     */
    public ArrayList<Article> getArticles(ArticleSearch articleSearch) {
        if (articleSearch != null) {

            try {
                ArrayList<Article> articles = articleSearch.getResponse().getArticles();
                return articles;
            } catch (Exception e) {

            }
        }

        return null;
    }
}
