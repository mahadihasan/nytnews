package com.hasan.app.nytnews.view_models;

import com.hasan.app.nytnews.models.Article;
import com.hasan.app.nytnews.models.ArticleSearch;
import com.hasan.app.nytnews.models.Response;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by mahadi on 19.06.17.
 */
public class MainViewModelTest {

    private ArticleSearch articleSearch;
    private MainViewModel viewModel;

    @Before
    public void initObject()
    {
        viewModel = new MainViewModel(null);
        ArrayList<Article> articles = new ArrayList<>();
        for(int i=0; i<10; i++)
        {
            Article article = new Article();
            article.setId("id"+i);
            articles.add(article);
        }
        Response response = new Response();
        response.setArticles(articles);
        articleSearch = new ArticleSearch();
        articleSearch.setResponse(response);
    }

    @Test
    public void getArticles() throws Exception {


        ArrayList<Article> articlesNew = viewModel.getArticles(articleSearch);

        assertEquals(articlesNew, articleSearch.getResponse().getArticles());
    }

}